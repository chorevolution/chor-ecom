package eu.chorevolution.gettingstarted.prosumer.paymentsystem.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.gettingstarted.provider.paymentsystem.PaymentInformation;
import eu.chorevolution.gettingstarted.provider.paymentsystem.PaymentSystemPT;

@Component(value="PaymentSystemPTImpl")
public class PaymentSystemPTImpl implements PaymentSystemPT{

	private static Logger LOGGER = LoggerFactory.getLogger(PaymentSystemPTImpl.class);

	@Override
	public void payment(PaymentInformation parameters) {
		
		LOGGER.info("*******PAYMENT SYSTEM*******");
		LOGGER.info("*         Paying 			*");
		LOGGER.info("* Client "+parameters.getClientInfo().getSurname()+" "+parameters.getClientInfo().getName()+" *");	
		LOGGER.info("* Credit Card Number "+ parameters.getClientInfo().getCreditCard().getNumber() +" *");
		LOGGER.info("* Credit Card Circuit Type "+ parameters.getClientInfo().getCreditCard().getPaymentCircuit() +" *");	
		LOGGER.info("* Payment amount "+ parameters.getTotalAmount() +" *");
		LOGGER.info("*******PAYMENT SYSTEM*******");
	}




}
