package eu.chorevolution.gettingstarted.provider.carrier.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.gettingstarted.provider.carrier.CarrierPT;
import eu.chorevolution.gettingstarted.provider.carrier.DeliveryPlan;
import eu.chorevolution.gettingstarted.provider.carrier.PackageDetails;

@Component(value="CarrierPTImpl")
public class CarrierPTImpl implements CarrierPT{

	private static Logger LOGGER = LoggerFactory.getLogger(CarrierPTImpl.class);
	
	@Override
	public void arrangeDelivery(DeliveryPlan parameters) {
		
		LOGGER.info("*******CARRIER*******");
		LOGGER.info("*      Arranging Delivery 	  *");
		LOGGER.info("* Tracking Number "+ parameters.getTrackingNumber() +" *");
		LOGGER.info("* Packages Number "+ parameters.getPackagesNumber() +" *");		
		for (PackageDetails packageDetails : parameters.getPackagesDetails()) {
			LOGGER.info("* Package ID "+ packageDetails.getId() +" *");	
			LOGGER.info("* Package Weight "+ packageDetails.getWeight() +" *");	
			LOGGER.info("* Package Height "+ packageDetails.getDimensions().getHeight() +" *");	
			LOGGER.info("* Package Width "+ packageDetails.getDimensions().getWidth() +" *");	
			LOGGER.info("* Package Depth "+ packageDetails.getDimensions().getDepth() +" *");				
		}
		LOGGER.info("* Address: "+ parameters.getAddress().getCity()+", "+parameters.getAddress().getStreet()+", "+parameters.getAddress().getPostcode()+" *");			
		LOGGER.info("* Delivery Service Type "+ parameters.getDeliveryService() +" *");	
		LOGGER.info("* Departure date "+ parameters.getDepartureDate() +" *");
		LOGGER.info("* Arrival date "+ parameters.getArrivalDate() +" *");	
		LOGGER.info("*******CARRIER*******");		
	}

}
