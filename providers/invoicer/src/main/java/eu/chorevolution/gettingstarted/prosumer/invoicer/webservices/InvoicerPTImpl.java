package eu.chorevolution.gettingstarted.prosumer.invoicer.webservices;

import java.util.Calendar;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.gettingstarted.provider.invoicer.Invoice;
import eu.chorevolution.gettingstarted.provider.invoicer.InvoiceRequest;
import eu.chorevolution.gettingstarted.provider.invoicer.InvoicerPT;
import eu.chorevolution.gettingstarted.provider.invoicer.OrderType;
import eu.chorevolution.gettingstarted.provider.invoicer.ProductInvoiceDetails;

@Component(value="InvoicerPTImpl")
public class InvoicerPTImpl implements InvoicerPT{

	private static Logger LOGGER = LoggerFactory.getLogger(InvoicerPTImpl.class);

	@Override
	public Invoice invoice(InvoiceRequest parameters) {
		
		LOGGER.info("*******INVOICER*******");
		LOGGER.info("*      Creating Invoice 	  *");
		Invoice invoice = new Invoice();
		invoice.setClientInfo(parameters.getClientInfo());
		invoice.setInvoiceDate(Calendar.getInstance().getTime());
		invoice.setInvoiceNumber(UUID.randomUUID().toString());
		invoice.setOrderID(parameters.getOrderID());	
		invoice.setClientInfo(parameters.getClientInfo());
		for (ProductInvoiceDetails productInvoiceDetails : parameters.getProductsInvoice()) {
			invoice.getProductsInvoice().add(productInvoiceDetails);
		}
		invoice.setOrderType(parameters.getOrderType());
    	double orderPrice = 0.0;
    	if(parameters.getOrderType().equals(OrderType.NORMAL)){
    		orderPrice = 5.99;
    	}
    	if(parameters.getOrderType().equals(OrderType.FAST)){
    		orderPrice = 7.99;
    	}
    	if(parameters.getOrderType().equals(OrderType.PREMIUM)){
    		orderPrice = 9.99;
    	} 
    	invoice.setOrderTypePrice(orderPrice);
		invoice.setVatPercentage(20.00);
		invoice.setVatAmount(parameters.getTotalAmount()/100*20);
		invoice.setTotalAmount(parameters.getTotalAmount());
		LOGGER.info("* Invoice Number "+ invoice.getInvoiceNumber() +" *");		
		LOGGER.info("* Invoice Date "+ invoice.getInvoiceDate() +" *");		
		LOGGER.info("* Order ID "+ invoice.getOrderID() +" *");	
		LOGGER.info("* Client "+invoice.getClientInfo().getSurname()+" "+invoice.getClientInfo().getName()+" *");
		for (ProductInvoiceDetails pInvoiceDetails : invoice.getProductsInvoice()) {
			LOGGER.info("* Product ID "+ pInvoiceDetails.getId() +" *");	
			LOGGER.info("* Product quantity "+ pInvoiceDetails.getQuantity() +" *");	
			LOGGER.info("* Product price "+ pInvoiceDetails.getPrice() +" *");	
		}
		LOGGER.info("* Order type "+ invoice.getOrderType() +" *");		
		LOGGER.info("* Order type price "+ invoice.getOrderTypePrice() +" *");
		LOGGER.info("* Vat percentage "+ invoice.getVatPercentage() +" *");
		LOGGER.info("* Vat amount "+ invoice.getVatAmount() +" *");
		LOGGER.info("* Total amount "+ invoice.getTotalAmount() +" *");
		LOGGER.info("*******INVOICER*******");
		return invoice;
	}

}
