package eu.chorevolution.gettingstarted.client.customer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.AddressDetails;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.CircuitType;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.ClientDetails;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.CreditCardDetails;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.CustomerPT;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.CustomerService;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.OrderDetails;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.OrderRequest;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.OrderRequestType;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.OrderResponse;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.OrderType;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.PackageDetails;
import eu.chorevolution.gettingstarted.client.customer.cdcustomer.client.ProductDetails;

public class Customer {
	
	private static Logger LOGGER = LoggerFactory.getLogger(Customer.class);

	public static void main(String[] args) throws InterruptedException, IOException {
		
		LOGGER.info("*******CUSTOMER*******");
		LOGGER.info("*      Order Product 	  *");
		CustomerService customerService = new CustomerService();
		CustomerPT customerPT = customerService.getCustomerPort();

		OrderRequestType orderRequestType = new OrderRequestType();		
		OrderRequest orderRequest = new OrderRequest();
		ClientDetails clientDetails = new ClientDetails();
		clientDetails.setId(7);
		clientDetails.setName("John");
		clientDetails.setSurname("Doe");
		clientDetails.setPhone("717-398-435");
		clientDetails.setEmail("johndoe@gmail.com");
				
		AddressDetails addressDetails = new AddressDetails();
		addressDetails.setCity("New York");
		addressDetails.setPostcode(10007);
		addressDetails.setStreet("Seventh Avenue");
		clientDetails.setAddress(addressDetails);
		
		CreditCardDetails creditCardDetails = new CreditCardDetails();
		creditCardDetails.setNumber(827763249);
		creditCardDetails.setPaymentCircuit(CircuitType.AMERICANEXPRESS);
		clientDetails.setCreditCard(creditCardDetails);
		
		orderRequest.setClientInfo(clientDetails);
		
		LOGGER.info("*      Client Details 	  *");	
		LOGGER.info("* Client Name "+ clientDetails.getName() +" *");
		LOGGER.info("* Client Surname "+ clientDetails.getSurname() +" *");
		LOGGER.info("* Client Phone "+ clientDetails.getPhone() +" *");
		LOGGER.info("* Client Email "+ clientDetails.getEmail() +" *");
		LOGGER.info("* Client Email "+ clientDetails.getEmail() +" *");
		LOGGER.info("* City "+ clientDetails.getAddress().getCity() +" *");		
		LOGGER.info("* Street "+ clientDetails.getAddress().getStreet() +" *");	
		LOGGER.info("* Credit Card Number "+ clientDetails.getCreditCard().getNumber() +" *");	
		LOGGER.info("* Credit Card Payment Circuit "+ clientDetails.getCreditCard().getPaymentCircuit() +" *");	
		LOGGER.info("*      Client Details 	  *");	
		
		OrderDetails orderDetails = new OrderDetails();
		orderDetails.setAddress(addressDetails);
		orderDetails.setOrderType(OrderType.PREMIUM);
		
		ProductDetails product1 = new ProductDetails();
		product1.setId(1);
		product1.setName("Apple iPhone 7 128GB 4G Black");
		product1.setPrice(739.00);
		product1.setQuantity(2);
		product1.setWeight(136);
		orderDetails.getProducts().add(product1);
		ProductDetails product2 = new ProductDetails();
		product2.setId(2);
		product2.setName("Samsung Galaxy S8 64 GB Black");
		product2.setPrice(829.00);
		product2.setQuantity(2);	
		product2.setWeight(154);
		orderDetails.getProducts().add(product2);
		ProductDetails product3 = new ProductDetails();
		product3.setId(3);
		product3.setName("IVSO Samsung Galaxy S8 Case");
		product3.setPrice(8.95);
		product3.setQuantity(2);	
		product3.setPrice(91);
		product3.setWeight(141);
		orderDetails.getProducts().add(product3);
		ProductDetails product4 = new ProductDetails();
		product4.setId(4);
		product4.setName("USB Type C Nylon Braided Cord Fast Charger for Samsung S8");
		product4.setPrice(9.99);
		product4.setQuantity(2);	
		product4.setWeight(181);
		orderDetails.getProducts().add(product4);		
		ProductDetails product5 = new ProductDetails();
		product5.setId(5);
		product5.setName("iPhone 7 Glass Screen Protector");
		product5.setPrice(7.99);
		product5.setQuantity(2);
		product5.setWeight(100);
		orderDetails.getProducts().add(product5);
		ProductDetails product6 = new ProductDetails();
		product6.setId(6);
		product6.setName("iPhone Charger Coolreall Lightning Cable");
		product6.setPrice(10.99);
		product6.setQuantity(2);
		product6.setWeight(159);
		orderDetails.getProducts().add(product6);
		
		orderRequest.setOrderInfo(orderDetails);
		
		LOGGER.info("*      Order Details 	  *");	
		LOGGER.info("* Order Type "+ orderDetails.getOrderType() +" *");	
		for (ProductDetails productDetails : orderDetails.getProducts()) {
			LOGGER.info("* Product Name "+ productDetails.getName() +" *");	
			LOGGER.info("* Product Price "+ productDetails.getPrice() +" *");	
			LOGGER.info("* Product Quantity "+ productDetails.getQuantity() +" *");	
			LOGGER.info("* Product Weight "+ productDetails.getWeight() +" *");				
		}
		LOGGER.info("*      Order Details 	  *");			
		orderRequestType.setMessageData(orderRequest);

		LOGGER.info("*******CUSTOMER*******");
		
		SpinnerThread spinnerThread = new Customer().new SpinnerThread();
		spinnerThread.start();
        
		OrderResponse orderResponse = customerPT.orderProducts(orderRequestType);
		
		LOGGER.info("*******CUSTOMER*******");
		LOGGER.info("*      Order Information Processed	  *");	
		LOGGER.info("* Order ID "+ orderResponse.getOrder().getOrderID() +" *");	
		LOGGER.info("* Order Type "+ orderResponse.getOrder().getOrderType() +" *");
		LOGGER.info("* Client Name "+ orderResponse.getOrder().getClientInfo().getName() +" *");
		LOGGER.info("* Client Surname "+ orderResponse.getOrder().getClientInfo().getSurname() +" *");		
		LOGGER.info("*      Order Information 	  *");
		LOGGER.info("*      Invoice Information 	  *");
		LOGGER.info("* Invoice Number "+ orderResponse.getInvoice().getInvoiceNumber() +" *");
		LOGGER.info("* Invoice Date "+ orderResponse.getInvoice().getInvoiceDate() +" *");
		LOGGER.info("* Order ID "+ orderResponse.getInvoice().getOrderID() +" *");
		LOGGER.info("* Order Type Price "+ orderResponse.getInvoice().getOrderTypePrice() +" *");
		LOGGER.info("* Vat Percentage "+ orderResponse.getInvoice().getVatPercentage() +" *");	
		LOGGER.info("* Vat Amount "+ orderResponse.getInvoice().getVatAmount() +" *");			
		LOGGER.info("* Total Amount "+ orderResponse.getInvoice().getTotalAmount() +" *");		
		LOGGER.info("*      Invoice Information 	  *");
		LOGGER.info("*      Delivery Information 	  *");
		LOGGER.info("* Delivery Service Type "+ orderResponse.getDelivery().getDeliveryService() +" *");
		LOGGER.info("* Departure Date "+ orderResponse.getDelivery().getDepartureDate() +" *");
		LOGGER.info("* Arrival Date "+ orderResponse.getDelivery().getArrivalDate() +" *");
		LOGGER.info("*      Delivery Information 	  *");
		LOGGER.info("*      Shipping Information 	  *");
		LOGGER.info("* Tracking Number "+ orderResponse.getShipping().getTrackingNumber() +" *");
		LOGGER.info("* Delivery Service "+ orderResponse.getShipping().getDeliveryService() +" *");	
		LOGGER.info("*      Packages Information 	  *");
		LOGGER.info("* Total Packages Number "+ orderResponse.getShipping().getShippedPackages().getPackagesNumber() +" *");
		for (PackageDetails packageDetails : orderResponse.getShipping().getShippedPackages().getPackagesDetails()) {
			LOGGER.info("* Package ID "+ packageDetails.getId() +" *");	
			LOGGER.info("* Package Height "+ packageDetails.getDimensions().getHeight() +" *");	
			LOGGER.info("* Package Width "+ packageDetails.getDimensions().getWidth() +" *");	
			LOGGER.info("* Package Depth "+ packageDetails.getDimensions().getDepth() +" *");	
		}
		LOGGER.info("*      Packages Information	  *");
		LOGGER.info("*      Shipping Information 	  *");
		LOGGER.info("*******CUSTOMER*******");
		
		spinnerThread.join();
	}
	
    public class SpinnerThread extends Thread {

	    public void run(){
	        System.out.print("*******Processing Order-START*******");
	        String[] progessStringArray = {"▏","▏","▎","▎","▍","▍","▌","▋","▊","▉"};
	        String progressString = "";
	        for (int x=10 ; x <= 100 ; x+=10){
	        		if(x == 10){
	        			progressString += progessStringArray[(x/10)];
	        		}
	        		else{
	        			progressString += progessStringArray[(x/10)-1];
	        		}
	                String data = "\r" + progressString + " " + x +" % Order Processed";
	                try {
						System.out.write(data.getBytes());
		                Thread.sleep(400);
					} catch (IOException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        }  
	        System.out.print("\n");
	        System.out.println("*******Processing Order-END*******");
	    }
	}
      
}
