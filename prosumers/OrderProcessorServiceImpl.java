package eu.chorevolution.prosumer.orderprocessor.business.impl.service;

import java.awt.image.PackedColorModel;
import java.util.Iterator;
import java.util.UUID;

import org.springframework.stereotype.Service;

import eu.chorevolution.prosumer.orderprocessor.DeliveryData;
import eu.chorevolution.prosumer.orderprocessor.DeliveryPlan;
import eu.chorevolution.prosumer.orderprocessor.DimensionsType;
import eu.chorevolution.prosumer.orderprocessor.Invoice;
import eu.chorevolution.prosumer.orderprocessor.InvoiceRequest;
import eu.chorevolution.prosumer.orderprocessor.OrderData;
import eu.chorevolution.prosumer.orderprocessor.OrderRequest;
import eu.chorevolution.prosumer.orderprocessor.OrderResponse;
import eu.chorevolution.prosumer.orderprocessor.OrderType;
import eu.chorevolution.prosumer.orderprocessor.PackageDetails;
import eu.chorevolution.prosumer.orderprocessor.PackagesData;
import eu.chorevolution.prosumer.orderprocessor.PaymentInformation;
import eu.chorevolution.prosumer.orderprocessor.PaymentInformationResponse;
import eu.chorevolution.prosumer.orderprocessor.ProductDetails;
import eu.chorevolution.prosumer.orderprocessor.ProductInvoiceDetails;
import eu.chorevolution.prosumer.orderprocessor.ProductScheduleDetails;
import eu.chorevolution.prosumer.orderprocessor.ScheduleRequest;
import eu.chorevolution.prosumer.orderprocessor.ScheduleResponse;
import eu.chorevolution.prosumer.orderprocessor.ShippingData;
import eu.chorevolution.prosumer.orderprocessor.ShippingRequest;
import eu.chorevolution.prosumer.orderprocessor.ShippingResponse;
import eu.chorevolution.prosumer.orderprocessor.business.ChoreographyInstanceMessages;
import eu.chorevolution.prosumer.orderprocessor.business.OrderProcessorService;

@Service
public class OrderProcessorServiceImpl implements OrderProcessorService {

	@Override
	public void orderProducts(OrderRequest parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of OrderRequest message from senderParticipantName
		*/
	}     

	@Override
	public void notifyShippingInformation(ShippingResponse parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of ShippingResponse message from senderParticipantName
		*/
	}     

    @Override
    public ScheduleRequest createScheduleRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    	
    	OrderRequest orderRequest = (OrderRequest) choreographyInstanceMessages.getMessage("orderRequest");    	
    	ScheduleRequest result = new ScheduleRequest();
    	result.setAddress(orderRequest.getOrderInfo().getAddress());
    	result.setOrderID(UUID.randomUUID().toString());
    	for (ProductDetails productDetails : orderRequest.getOrderInfo().getProducts()) {
    		ProductScheduleDetails productScheduleDetails = new ProductScheduleDetails();
    		productScheduleDetails.setId(productDetails.getId());
    		productScheduleDetails.setQuantity(productDetails.getQuantity());
    		result.getProductsSchedule().add(productScheduleDetails);
		}  	
    	/**
		 *	TODO write the code that generates ScheduleRequest message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    	return result;
    }
    
    @Override
    public PaymentInformation createPaymentInformation(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    	
    	OrderRequest orderRequest = (OrderRequest) choreographyInstanceMessages.getMessage("orderRequest");
    	PaymentInformation result = new PaymentInformation();
    	result.setClientInfo(orderRequest.getClientInfo());
    	Double totalAmount = 0.0;
    	for (ProductDetails productDetails : orderRequest.getOrderInfo().getProducts()) {
    		totalAmount += productDetails.getPrice() * productDetails.getQuantity();
		}
    	totalAmount+=totalAmount/100*20;
    	double orderPrice = 0.0;
    	if(orderRequest.getOrderInfo().getOrderType().equals(OrderType.NORMAL)){
    		orderPrice = 5.99;
    	}
    	if(orderRequest.getOrderInfo().getOrderType().equals(OrderType.FAST)){
    		orderPrice = 7.99;
    	}
    	if(orderRequest.getOrderInfo().getOrderType().equals(OrderType.PREMIUM)){
    		orderPrice = 9.99;
    	}    	
    	totalAmount+=orderPrice;
    	result.setTotalAmount(totalAmount);
    	/**
		 *	TODO write the code that generates PaymentInformation message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    	return result;
    }
    
    @Override
    public InvoiceRequest createInvoiceRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    	
    	OrderRequest orderRequest = (OrderRequest) choreographyInstanceMessages.getMessage("orderRequest");
    	PaymentInformation paymentInformation = (PaymentInformation) choreographyInstanceMessages.getMessage("paymentInformation");
    	ScheduleRequest scheduleRequest = (ScheduleRequest) choreographyInstanceMessages.getMessage("scheduleRequest");
    	InvoiceRequest result = new InvoiceRequest();
    	result.setOrderID(scheduleRequest.getOrderID());
    	result.setClientInfo(orderRequest.getClientInfo());  	
    	for (ProductDetails productDetails : orderRequest.getOrderInfo().getProducts()) {
    		ProductInvoiceDetails productInvoiceDetails = new ProductInvoiceDetails();
    		productInvoiceDetails.setId(productDetails.getId());
    		productInvoiceDetails.setPrice(productDetails.getPrice());
    		productInvoiceDetails.setQuantity(productDetails.getQuantity());
			result.getProductsInvoice().add(productInvoiceDetails);
		}   	
    	result.setOrderType(orderRequest.getOrderInfo().getOrderType());
    	result.setTotalAmount(paymentInformation.getTotalAmount());
    	/**
		 *	TODO write the code that generates InvoiceRequest message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    	return result;
    }
    
    @Override
    public ShippingRequest createShippingRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    	
    	ScheduleResponse scheduleResponse = (ScheduleResponse) choreographyInstanceMessages.getMessage("scheduleResponse");
    	OrderRequest orderRequest = (OrderRequest) choreographyInstanceMessages.getMessage("orderRequest");   	
    	ShippingRequest result = new ShippingRequest();
    	result.setOrderID(scheduleResponse.getOrderID()); 	
    	int packageID = 0;
    	double productsPackageTotalWeight = 0;
    	int packagesNumber = 0;    	
    	for (ProductDetails productDetails : orderRequest.getOrderInfo().getProducts()) {
			if(productsPackageTotalWeight < 200){
				productsPackageTotalWeight+=productDetails.getWeight()*productDetails.getQuantity();
			}
			else{
				packagesNumber++;
		    	PackageDetails packageDetails = new PackageDetails();
		    	packageDetails.setId(++packageID);	
		    	DimensionsType dimensionsType = new DimensionsType();
		    	dimensionsType.setDepth(35.0);
		    	dimensionsType.setHeight(31.0);
		    	dimensionsType.setWidth(52.5);
		    	packageDetails.setDimensions(dimensionsType);
		    	result.getPackagesDetails().add(packageDetails);
		    	productsPackageTotalWeight = productDetails.getWeight()*productDetails.getQuantity();
			}
		}
    	result.setPackagesNumber(packagesNumber);
    	result.setAddress(orderRequest.getOrderInfo().getAddress());
    	result.setDepartureDate(scheduleResponse.getScheduleDetails().getDepartureDate());
    	result.setArrivalDate(scheduleResponse.getScheduleDetails().getArrivalDate());
    	/**
		 *	TODO write the code that generates ShippingRequest message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    	return result;
    }
    
    @Override
    public OrderResponse createOrderResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    	
    	OrderRequest orderRequest = (OrderRequest) choreographyInstanceMessages.getMessage("orderRequest");
    	ScheduleResponse scheduleResponse = (ScheduleResponse) choreographyInstanceMessages.getMessage("scheduleResponse");
    	ShippingResponse shippingResponse = (ShippingResponse) choreographyInstanceMessages.getMessage("shippingResponse");
    	Invoice invoice = (Invoice) choreographyInstanceMessages.getMessage("invoice");
    	OrderResponse result = new OrderResponse();
    	
    	OrderData orderData = new OrderData();
    	orderData.setClientInfo(orderRequest.getClientInfo());
    	orderData.setOrderID(scheduleResponse.getOrderID());
    	orderData.setOrderType(orderRequest.getOrderInfo().getOrderType());
    	result.setOrder(orderData);
    	
    	ShippingData shippingData = new ShippingData();
    	shippingData.setDeliveryService(shippingResponse.getDeliveryService()); 	
    	PackagesData packagesData = new PackagesData();
    	packagesData.setPackagesNumber(shippingResponse.getPackagesNumber());
    	for (PackageDetails packageDetails : shippingResponse.getPackagesDetails()) {
        	packagesData.getPackagesDetails().add(packageDetails);			
		}	
    	shippingData.setTrackingNumber(shippingResponse.getTrackingNumber());
    	shippingData.setShippedPackages(packagesData);
    	result.setShipping(shippingData);
    	
    	DeliveryData deliveryData = new DeliveryData();
    	deliveryData.setDepartureDate(scheduleResponse.getScheduleDetails().getDepartureDate());
    	deliveryData.setArrivalDate(scheduleResponse.getScheduleDetails().getArrivalDate());
    	deliveryData.setDeliveryService(shippingResponse.getDeliveryService());
    	result.setDelivery(deliveryData);
    	
    	result.setInvoice(invoice);
    	/**
		 *	TODO write the code that generates OrderResponse message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    	return result;
    }
    
	@Override    
    public void receiveScheduleResponse(ScheduleResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (ScheduleResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveInvoice(Invoice parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (Invoice message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }

	@Override
	public void receivePaymentInformationResponse(PaymentInformationResponse parameter, String choreographyTaskName,
			String senderParticipantName) {
		// TODO Auto-generated method stub
		
	}
    

}
